package marcelo.breguenait.breedinghelper;

class InterfaceAbility {
    final public String abilityName;
    final public int abilitySlot;

    public InterfaceAbility(String abilityName, int abilitySlot) {
        this.abilityName = abilityName;
        this.abilitySlot = abilitySlot;
    }
}
